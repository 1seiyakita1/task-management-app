class TasksController < ApplicationController
  before_action :set_task, only: %i[show edit update destroy]

  def index
    @q = current_user.tasks.ransack(params[:q])
    @tasks = @q.result(distinct: true).page(params[:page]).recent

    respond_to do |format|
      format.html
      format.csv { send_data @tasks.generate_csv, filename: "tasks-#{Time.zone.now.strftime('%Y%m%d%S')}.csv" }
    end
  end

  def show; end

  def new
    @task = Task.new
  end

  def edit; end

  def confirm_new
    @task = current_user.tasks.new(task_params)
    render :new unless @task.valid?
  end

  def create
    @task = current_user.tasks.new(task_params)

    if in_a_case_other_than_image_files
      let_users_select_images
    elsif @task.save
      TaskMailer.creation_email(@task).deliver_now
      SampleJob.perform_later
      redirect_to @task, success: "タスク「#{@task.name}」を登録しました。"
    else
      render :new
    end
  end

  def update
    if in_a_case_other_than_image_files
      let_users_select_images
    else
      @task.update!(task_params)
      redirect_to tasks_url, success: "タスク「#{@task.name}」を更新しました。"
    end
  end

  def destroy
    @task.destroy
    redirect_to tasks_url, success: "タスク「#{@task.name}」を削除しました。"
  end

  def ajax_destroy
    @task = Task.find(params[:id])
    @task.destroy
  end

  def import
    if params[:file].nil?
      redirect_to tasks_url, danger: 'ファイルを選択してください'
    elsif !params[:file].content_type.include? 'csv'
      redirect_to tasks_url, danger: 'CSVファイルを選択してください'
    else
      current_user.tasks.import(params[:file])
      redirect_to tasks_url, success: 'タスクを追加しました'
    end
  end

  private

  def task_params
    params.require(:task).permit(:name, :description, :image)
  end

  def set_task
    @task = current_user.tasks.find(params[:id])
  end

  def not_image_file
    @task = current_user.tasks.find(params[:id])
  end

  def in_a_case_other_than_image_files
    !params[:task][:image].nil? && !params[:task][:image].content_type.include?('image')
  end

  def let_users_select_images
    flash.now[:danger] = '画像ファイルを選択してください'
    render action: :new
  end
end
