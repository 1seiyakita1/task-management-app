#初期データ
User.find_or_create_by!(email: 'admin@admin.com', id: 1) do |user|
  user.id = 1
  user.name = 'admin'
  user.password = 'hogehoge'
  user.password_confirmation = 'hogehoge'
  user.admin = true
end

(1..100).each do |i|
  Task.create!(name: "サンプルタスク(#{i})",
               user_id: 1 )
end
